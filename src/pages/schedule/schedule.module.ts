import { SchedulePage } from './schedule' ;
import { NgModule } from '@angular/core' ;
import { IonicPageModule } from 'ionic-angular';
import { NgCalendarModule  } from 'ionic2-calendar';


@NgModule({
	declarations:[
	SchedulePage
	],
	imports: [
	IonicPageModule.forChild(SchedulePage),
	NgCalendarModule
	],
	exports: [
	SchedulePage
	]
}) 

export class ScheduleModule {
	
}