import { TrendsPage } from './trends' ;
import { NgModule } from '@angular/core' ;
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations:[
	TrendsPage
	],
	imports: [
	IonicPageModule.forChild(TrendsPage)
	],
	exports: [
	TrendsPage
	]
}) 

export class TrendsModule {
	
}