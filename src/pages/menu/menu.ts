import { Component , ViewChild} from '@angular/core';
import { IonicPage , NavController, NavParams, Nav } from 'ionic-angular';

export interface PageInterface {
	title: string ;
	pageName: string;
	tabsComponent?:any ;
	index?: number ;
	icon: string ;
}

@IonicPage()

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {


	rootPage = 'TabsPage';

	@ViewChild(Nav) nav: Nav ;

	pages :PageInterface[] = [
	{ title: 'Guide' , pageName: 'TabsPage',tabsComponent:'Tab1Page',index:0,icon:'home'},
	{ title: '*Diet' , pageName: 'TabsPage',tabsComponent:'Tab1Page',index:1,icon:'contacts'},
	{ title: '*Workout' , pageName: 'TabsPage',tabsComponent:'Tab2Page',index:1,icon:'contacts'},
	{ title: '*Consultation' , pageName: 'TabsPage',tabsComponent:'Tab2Page',index:1,icon:'contacts'},
	{ title: '*Medical Check-up' , pageName: 'TabsPage',tabsComponent:'Tab2Page',index:1,icon:'contacts'},
	{ title: 'Motivation' , pageName: 'SpecialPage',icon:'home'},
  { title: 'Heal' , pageName: 'GoalsPage',icon:'home'},
  { title: 'Policy Details' , pageName: 'TrendsPage' , icon: 'home'},
  { title: 'Log Out' , pageName: 'LoginPage' , icon: 'home'}
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  isActive(page:PageInterface){

  }

  openPage(page:PageInterface){
  		let params: {} ;

  		params = { tabIndex : page.index}
  
  		if(this.nav.getActiveChildNav() && page.index != undefined)
  		{
  			this.nav.getActiveChildNav().select(page.index);
  		}else {
  			this.nav.setRoot(page.pageName,params);
  		}
  }

}
