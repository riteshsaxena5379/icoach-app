import { SpecialPage } from './special' ;
import { NgModule } from '@angular/core' ;
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations:[
	SpecialPage
	],
	imports: [
	IonicPageModule.forChild(SpecialPage)
	],
	exports: [
	SpecialPage
	]
}) 

export class SpecialPageModule {
	
}