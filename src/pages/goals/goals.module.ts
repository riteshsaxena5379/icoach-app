import { GoalsPage } from './goals' ;
import { NgModule } from '@angular/core' ;
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations:[
	GoalsPage
	],
	imports: [
	IonicPageModule.forChild(GoalsPage)
	],
	exports: [
	GoalsPage
	]
}) 

export class GoalsPageModule {
	
}